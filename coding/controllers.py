import json
from persistence.dao import TestDAO
from persistence.models import Test
from challenges.sync_shopping import SyncShopping

ERRORS = {
    "500": {"code":500, "status":"Exception Ocurred"},
    "404": {"code":404, "status":"Not Found"}
}

class TestController(object):

    def create(self,data):
        try:
            result = SyncShopping(data['input']).solve()
            test_data = '"input":%s,"result":%d' %(data["input"], result)
            test = Test(test=test_data.replace("'",'"'))
            TestDAO().persist(test)
            data_json = json.loads(test.to_json())
            data_json["code"] = 200
            data_json["status"] = "OK"
            data_json["message"] = "Test %d has been created!" %test.id
        except Exception as e:
            data_json = ERRORS["500"]
            data_json["message"] = "An exception has occurred: %s!" %e
        return data_json

    def retrieve_all(self):
        tests = []
        for test in TestDAO().findAll():
            tests.append(json.loads(Test(test[0], test[1]).to_json()))
        return {"count":len(tests), "tests": tests}

    def retrieve(self, id):
        test = TestDAO().find(id)
        if test:
            data = json.loads(Test(test[0], test[1]).to_json())
        else:
            data = ERRORS["404"]
            data["message"] = "Test %d has not been found!"%id
        return data

    def update(self, id, data):
        if TestDAO().find(id):
            try:
                result = SyncShopping(data['input']).solve()
                test_data = '"input":%s,"result":%d' %(data["input"], result)
                test = Test(id, test_data.replace("'",'"'))
                TestDAO().update(test)
                data_json = json.loads(test.to_json())
                data_json["code"] = 200
                data_json["status"] = "OK"
                data_json["message"] = "Test %d has been updated!" %test.id
            except Exception as e:
                data_json = ERRORS["500"]
                data_json["message"] = "An exception has occurred: %s!" %e
        else:
            data_json = ERRORS["404"]
            data_json["message"] = "Test %d has not been found!"%id
        return data_json

    def delete(self, id):
        test = TestDAO().find(id)
        if test:
            TestDAO().delete(id)
            result = {
                "code":200,
                "status":"OK", 
                "message":"Test %d has been deleted!"%id
            }
        else:
            result = ERRORS["404"]
            result["message"] = "Test %d has not been found!" %id
        return result
