import requests
import unittest

URL = "http://127.0.0.1:8080"

class UpdateTestCase(unittest.TestCase):

    def update(self, input_data, success=True):
        if success:
            response = requests.get("%s/challenge/tests/" %URL)
            tests = response.json()["tests"]
            id = tests[0]["id"] if tests else 0
        else:
            id = 0
        response = requests.put(
            "%s/challenge/tests/%d" %(URL,id),
            json = input_data
        )
        return response

    def test_update(self):
        input_data = {
            "input":[
                "6 5 4", "1 2", "1 2", "1 1", "2 3 4", "2 3 4", "1 4",
                "5 4 646", "4 1 997", "2 1 881", "2 6 114", "3 1 46"
            ]
        }
        response = self.update(input_data)
        self.assertEqual(response.status_code, 200, "Unexpected error")

    def test_update_correct_answer(self):
        input_data = {
            "input": [
                "6 5 4", "1 2", "1 2", "1 1", "2 3 4", "2 3 4", "1 4",
                "5 4 646", "4 1 997", "2 1 881", "2 6 114", "3 1 46"
            ]
        }
        response = self.update(input_data)
        self.assertEqual(response.json()["result"], 2989, "Incorrect result!")

    def test_update_not_found(self):
        input_data = {
            "input":[
                "6 5 4", "1 2", "1 2", "1 1", "2 3 4", "2 3 4", "1 4",
                "5 4 646", "4 1 997", "2 1 881", "2 6 114", "3 1 46"
            ]
        }
        response = self.update(input_data, success=False)
        self.assertEqual(response.status_code, 404, response.json()["message"])

    def test_update_bad_input(self):
        input_data = {"input": ["6 5 4", "1 2", "1 2", "1 1", "2 3 4", "2 3 4"]}
        response = self.update(input_data)
        self.assertEqual(response.status_code, 500, response.json()["message"])

    def test_update_bad_input_str(self):
        input_data = {"input":["foo", "bar"]}
        response = self.update(input_data)
        self.assertEqual(response.status_code, 500, response.json()["message"])

    def test_update_input_not_found(self):
        input_data = {}
        response = self.update(input_data)
        self.assertEqual(response.status_code, 500, response.json()["message"])

    def test_update_input_empty(self):
        input_data = {"input":[]}
        response = self.update(input_data)
        self.assertEqual(response.status_code, 500, response.json()["message"])
