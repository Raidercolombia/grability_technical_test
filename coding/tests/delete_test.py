import requests
import unittest

URL = "http://127.0.0.1:8080"

class DeleteTestCase(unittest.TestCase):

    def test_delete(self):
        response = requests.get("%s/challenge/tests/" %URL)
        tests = response.json()["tests"]
        if tests:
            response = requests.delete(
                "%s/challenge/tests/%d" %(URL, tests[0]["id"])
            )
        self.assertEqual(response.status_code, 200, "Unexpected error")

    def test_delete_not_found(self):
        response = requests.delete("%s/challenge/tests/0" %URL)
        self.assertEqual(response.status_code, 404, response.json()["message"])


    def test_delete_str(self):
        response = requests.delete("%s/challenge/tests/id" %URL)
        self.assertEqual(response.status_code, 404, response.text)
