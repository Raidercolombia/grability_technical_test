import requests
import unittest

URL = "http://127.0.0.1:8080"

class RetrieveTestCase(unittest.TestCase):

    def test_retrieve(self):
        response = requests.get("%s/challenge/tests/" %URL)
        tests = response.json()["tests"]
        if tests:
            response = requests.get(
                "%s/challenge/tests/%d" %(URL, tests[0]["id"])
            )
        self.assertEqual(response.status_code, 200, "Unexpected error")

    def test_retrieve_not_found(self):
        response = requests.get("%s/challenge/tests/0" %URL)
        self.assertEqual(response.status_code, 404, response.json()["message"])

    def test_retrieve_all(self):
        response = requests.get("%s/challenge/tests/" %URL)
        self.assertEqual(
                        response.status_code, 200,
                        "There are %d tests!" %response.json()["count"])

    def test_retrieve_str(self):
        response = requests.get("%s/challenge/tests/id" %URL)
        self.assertEqual(response.status_code, 404, response.text)
