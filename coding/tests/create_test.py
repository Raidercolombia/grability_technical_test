import requests
import unittest

URL = "http://127.0.0.1:8080"

class CreateTestCase(unittest.TestCase):

    def create(self, input_data):
        response = requests.post(
            "%s/challenge/test/" %URL,
            json = input_data
        )
        return response

    def test_create(self):
        input_data = {
            "input": [
                "5 5 5", "1 1", "1 2", "1 3", "1 4", "1 5", "1 2 10", "1 3 10",
                "2 4 10", "3 5 10", "4 5 10"
            ]
        }
        response = self.create(input_data)
        self.assertEqual(response.status_code, 200, response.json()["message"])

    def test_create_long_input(self):
        input_data = {
            "input":[
                "6 10 3", "2 1 2", "1 3", "0", "2 1 3", "1 2", "1 3", "1 2 572",
                "4 2 913", "2 6 220", "1 3 579", "2 3 808", "5 3 298", "6 1 927",
                "4 5 171", "1 5 671", "2 5 463"
            ]
        }
        response = self.create(input_data)
        self.assertEqual(response.json()["result"], 792, "Incorrect result!")

    def test_create_bad_input(self):
        input_data = {"input": ["5 5 5", "1 1", "1 2", "1 3", "1 4", "1 5"]}
        response = self.create(input_data)
        self.assertEqual(response.status_code, 500, response.json()["message"])

    def test_create_bad_input_str(self):
        input_data = {"input":["foo", "bar"]}
        response = self.create(input_data)
        self.assertEqual(response.status_code, 500, response.json()["message"])

    def test_create_correct_answer(self):
        input_data = {
            "input": [
                "5 5 5", "1 1", "1 2", "1 3", "1 4", "1 5", "1 2 10", "1 3 10",
                "2 4 10", "3 5 10", "4 5 10"
            ]
        }
        response = self.create(input_data)
        self.assertEqual(response.json()["result"], 30, "Incorrect result!")

    def test_create_input_not_found(self):
        response = requests.post("%s/challenge/test/" %URL)
        self.assertEqual(response.status_code, 500, response.json()["message"])

    def test_create_input_empty(self):
        input_data = {"input":[]}
        response = self.create(input_data)
        self.assertEqual(response.status_code, 500, response.json()["message"])
