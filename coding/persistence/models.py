import json

class Test(object):
    def __init__(self, id=0, test=""):
        self.id = id
        self.test = test

    def set_id(self, id):
        self.id = id

    def to_json(self):
        return json.dumps(json.loads('{"id":%s,%s}' %(self.id,self.test)))
