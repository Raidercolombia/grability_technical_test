import psycopg2

DATABASE = "grability"

class Connection(object):

    def __init__(self):
        self._dbname = DATABASE

    def open(self):
        self._conn = psycopg2.connect("dbname=%s user=postgres" %self._dbname)

    def close(self):
        self._conn.close()

    def get_connection(self):
        return self._conn

    def get_cursor(self):
        return self._conn.cursor()
