from .driver import Connection
from .models import Test

class TestDAO(object):
    def __init__(self):
        self._connection = Connection()

    def findAll(self):
        self._connection.open()
        cursor = self._connection.get_cursor()
        cursor.execute("SELECT * FROM Test;")
        result_set = cursor.fetchall()
        cursor.close()
        self._connection.close()
        return result_set

    def find(self, id):
        self._connection.open()
        cursor = self._connection.get_cursor()
        cursor.execute("SELECT * FROM Test WHERE id=%s;", (id,))
        test = cursor.fetchone()
        cursor.close()
        self._connection.close()
        return test

    def persist(self, test):
        self._connection.open()
        cursor = self._connection.get_cursor()
        cursor.execute(
            "INSERT INTO test (test_data) VALUES (%s) RETURNING id;",(test.test,)
        )
        self._connection.get_connection().commit()
        test.set_id(cursor.fetchone()[0])
        cursor.close()
        self._connection.close()

    def update(self, test):
        self._connection.open()
        cursor = self._connection.get_cursor()
        cursor.execute(
            "UPDATE test SET test_data=%s WHERE id=%s;",(test.test,test.id,)
        )
        self._connection.get_connection().commit()
        cursor.close()
        self._connection.close()

    def delete(self, id):
        self._connection.open()
        cursor = self._connection.get_cursor()
        cursor.execute("DELETE FROM Test WHERE id=%s;", (id,))
        self._connection.get_connection().commit()
        cursor.close()
        self._connection.close()
