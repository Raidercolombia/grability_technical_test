﻿-- DROP DATABASE grability;

CREATE DATABASE grability
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1;

-- DROP TABLE public.grability;

CREATE TABLE public.test
(
  id serial NOT NULL,
  test_data text,
  CONSTRAINT test_pk PRIMARY KEY (id)
)