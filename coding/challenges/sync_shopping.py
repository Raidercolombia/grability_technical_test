from heapq import heappush as push, heappop as pop

class Node(object):
    def __init__(self, fishes_sold):
        self.fishes_sold = fishes_sold
        self.roads = []

class SyncShopping(object):
    def __init__(self, input):
        self.num_shops, num_roads, num_fish_types = map(int, input[0].split())
        self.all_fishes = self.count_items(range(1, num_fish_types + 1))
        nodes = [None]
        for i in range(self.num_shops):
            shop_info = list(map(int, input[i+1].split()))
            if len(shop_info) == 1:
                nodes.append(Node(0))
            else:
                nodes.append(Node(self.count_items(shop_info[1:])))
        for i in range(num_roads):
            nodeA, nodeB, cost = map(int, input[i+1+self.num_shops].split())
            nodes[nodeA].roads.append((nodeB, cost))
            nodes[nodeB].roads.append((nodeA, cost))
        self.nodes = nodes

    def count_items(self, items):
        return sum([2**(i+1) for i in items])
            
    def solve(self):
        goal_set = set([])
        visited_set = set([])
        frontier_nodes = []
        push(frontier_nodes, (0, 1, self.nodes[1].fishes_sold))
        while frontier_nodes:
            cost, current, fishes_bought = pop(frontier_nodes)
            if (current, fishes_bought) not in visited_set:
                visited_set.add((current, fishes_bought))
            else:
                continue
            if current == self.num_shops:
                if fishes_bought not in goal_set:
                    if fishes_bought == self.all_fishes:
                        return cost
                    for goal in goal_set:
                        if fishes_bought | goal == self.all_fishes:
                            return cost
                    goal_set.add(fishes_bought)

            for neighbor, travel_time in self.nodes[current].roads:
                fishes_sold = self.nodes[neighbor].fishes_sold
                new_fishes_bought = fishes_bought | fishes_sold
                new_frontier_node = (cost + travel_time,
                                    neighbor,
                                    new_fishes_bought)
                push(frontier_nodes, new_frontier_node)
