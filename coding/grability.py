from bottle import request, response, get, post, put, delete
from bottledaemon import daemon_run
from controllers import TestController

@post('/challenge/test/')
def create_test():
    data = TestController().create(request.json)
    response.status = data["code"]
    return data

@get('/challenge/tests/<id:int>')
def retrieve(id):
    data = TestController().retrieve(id)
    if "code" in data:
        response.status = data["code"]
    return data

@get('/challenge/tests/')
def retrieve_all():
    data = TestController().retrieve_all()
    return data

@put('/challenge/tests/<id:int>')
def update_test(id):
    data = TestController().update(id, request.json)
    response.status = data["code"]
    return data

@delete('/challenge/tests/<id:int>')
def delete_test(id):
    data = TestController().delete(id)
    response.status = data["code"]
    return data

if __name__ == "__main__":
    daemon_run(host="0.0.0.0", port=8080)
