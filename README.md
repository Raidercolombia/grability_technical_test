# README #
Repository for apply to the **Backend Developer Semi - Senior** job in Grability Inc.

# DEPENDENCIES #
**Bottle**: [Bottle](https://bottlepy.org/docs/dev/) is a fast, simple and lightweight WSGI micro web-framework for Python.
`pip install bottle`

**Bottledaemon**: A [simple tool](https://pypi.python.org/pypi/BottleDaemon) to help daemonize bottle applications.
`pip install bottledaemon`

**Psycopg2**: [Psycopg](http://initd.org/psycopg/) is the most popular PostgreSQL adapter for the Python programming language.
`pip install psycopg2`

**Requests**: [Requests](http://docs.python-requests.org/en/master/) allows you to send organic, grass-fed HTTP/1.1 requests, without the need for manual labor.
`pip install requests`

# USAGE #
**Start**: `python grability.py start`
**Stop**: `python grability.py stop`

# TESTS #
`python -m unittest discover tests/ -v -p "\*\_test.py"`
